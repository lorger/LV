# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
import sklearn.metrics as mt


def plot_confusion_matrix(c_matrix):

    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)
        
    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')
    width = len(c_matrix)
    height = len(c_matrix[0])
    for x in xrange(width):
        for y in xrange(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x), horizontalalignment='center', verticalalignment='center', color = 'green', size = 20)
    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])

    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()

def generate_data(n):
     #prva klasa
     n1 = n/2
     x1_1 = np.random.normal(0.0, 2, (n1,1));
     #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
     x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
     y_1 = np.zeros([n1,1])
     temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
     #druga klasa
     n2 = n - n/2
     x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
     y_2 = np.ones([n2,1])
     temp2 = np.concatenate((x_2,y_2),axis = 1)
    
     data = np.concatenate((temp1,temp2),axis = 0)
    
     #permutiraj podatke
     indices = np.random.permutation(n)
     data = data[indices,:]
    
     return data

#definiranje specificity funkcije
def specificity(c_matrix):
    return float(c_matrix[1][1])/float((c_matrix[1][1] + c_matrix[0][1])) 

#precision score ugrađena funkcija ne izbacuje tocni rezultat. izbacuje rezultat za specificity
def precision(c_matrix):
    return float(c_matrix[0][0])/float((c_matrix[0][0] + c_matrix[0][1]))  
    
#generiramo podatke za učenje     
np.random.seed(242)
train=generate_data(200)
np.random.seed(12)
test=generate_data(100)

#fit na podatke
X=train[:,0:2]
Y=train[:,2]
linearModel=lm.LogisticRegression()
linearModel.fit(X,Y)


#izračunavanje granice odluke
w = linearModel.coef_[0]
a = -w[0] / w[1]
x1 = np.linspace(-6, 6)
x2 = a * x1 - (linearModel.intercept_[0]) / w[1]

#klasifikacija skupa za test
Xt=test[:,0:2]
Yt=test[:,2]
test_rez=linearModel.predict(Xt)

#crtanje matrice zabune
c_matrix=mt.confusion_matrix(Yt,test_rez)
plot_confusion_matrix(c_matrix)

#pokazatelji 
print "Accuracy:", mt.accuracy_score(Yt,test_rez)
print "Misclassification rate:", 1-mt.accuracy_score(Yt,test_rez)
print "Precision:", precision(c_matrix)
print "Recall/sensitivity:", mt.recall_score(Yt,test_rez)
print "Specificity:", specificity(c_matrix)
