import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm

def generate_data(n):
     #prva klasa
     n1 = n/2
     x1_1 = np.random.normal(0.0, 2, (n1,1));
     #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
     x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
     y_1 = np.zeros([n1,1])
     temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
     #druga klasa
     n2 = n - n/2
     x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
     y_2 = np.ones([n2,1])
     temp2 = np.concatenate((x_2,y_2),axis = 1)
    
     data = np.concatenate((temp1,temp2),axis = 0)
    
     #permutiraj podatke
     indices = np.random.permutation(n)
     data = data[indices,:]
    
     return data

#generiramo podatke za učenje     
np.random.seed(242)
train=generate_data(200)
np.random.seed(12)
test=generate_data(100)

#fit na podatke
X=train[:,0:2]
Y=train[:,2]
linearModel=lm.LogisticRegression()
linearModel.fit(X,Y)


#izračunacanje granice odluke
w = linearModel.coef_[0]
a = -w[0] / w[1]
x1 = np.linspace(-6, 6)
x2 = a * x1 - (linearModel.intercept_[0]) / w[1]

#klasifikacija skupa za test
Xt=test[:,0:2]
Yt=test[:,2]
test_rez=linearModel.predict(Xt)

#crtanje testnog skupa
plt.figure()
for element in test:
    if element[2]==1:
        plt.scatter(element[0], element[1], c='b')
    else:
        plt.scatter(element[0], element[1], c='r')
plt.plot(x1, x2, 'k-')

#crtanje dobro klasificiranih uzoraka zelenom bojom, a lose crvenom
plt.figure()
for i in range(0,100):
    if test[i][2]==test_rez[i]:
        plt.scatter(test[i][0], test[i][1], c='g')
    else:
        plt.scatter(test[i][0], test[i][1], c='r')
plt.plot(x1, x2, 'k-')