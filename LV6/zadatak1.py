# -*- coding: utf-8 -*-
"""
Created on Mon Jan 30 18:12:45 2017

@author: Korisnik
"""

import numpy as np
from sklearn.neural_network import MLPClassifier
from sklearn import preprocessing
import matplotlib.pyplot as plt
import sklearn.metrics as mt
import sklearn.linear_model as lm
from sklearn.preprocessing import PolynomialFeatures



def plot_confusion_matrix(c_matrix):

    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)
        
    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')
    width = len(c_matrix)
    height = len(c_matrix[0])
    for x in xrange(width):
        for y in xrange(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x), horizontalalignment='center', verticalalignment='center', color = 'green', size = 20)
    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])

    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()


def generate_data(n):
    #prva klasa
    n1 = n/2
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = n - n/2
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n)
    data = data[indices,:]
    
    return data

np.random.seed(242)
generirani_podaci = generate_data(100)
prepro_podaci = preprocessing.scale(generirani_podaci[:,0:2])
podaci = np.c_[prepro_podaci, generirani_podaci[:,2]]


indeksi = np.random.permutation(len(podaci))
indeksi_trening = indeksi[0:int(np.floor(0.7*len(podaci)))]
indeksi_test = indeksi[int(np.floor(0.7*len(podaci))):len(podaci)]

trening = podaci[indeksi_trening]
test = podaci[indeksi_test]


clf = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(5, 2), random_state=1)
X=trening[:,0:2]
y=trening[:,2]

clf.fit(X,y)

h = 0.02  
x_min, x_max = trening[:, 0].min() - 1, trening[:, 0].max() + 1
y_min, y_max = trening[:, 1].min() - 1, trening[:, 1].max() + 1
x_mg, y_mg = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
koef=np.c_[x_mg.ravel(),y_mg.ravel()]

Z = clf.predict(koef)
Z = Z.reshape(x_mg.shape)


plt.figure()
for element in test:
    if element[2]==1:
        plt.scatter(element[0], element[1], c='c')
    else:
        plt.scatter(element[0], element[1], c='orange')
plt.contour(x_mg, y_mg, Z, cmap=plt.cm.Paired)

test_rez = clf.predict(test[:,0:2])

plt.figure()
for i in range(0,len(test)):
    if test[i][2]==test_rez[i]:
        plt.scatter(test[i][0], test[i][1], c='g')
    else:
        plt.scatter(test[i][0], test[i][1], c='r')
plt.contour(x_mg, y_mg, Z, cmap=plt.cm.Paired)

c_matrix=mt.confusion_matrix(test[:,2],test_rez)
plot_confusion_matrix(c_matrix)

print "Accuracy:", mt.accuracy_score(test[:,2],test_rez)
print "Misclassification rate:", 1-mt.accuracy_score(test[:,2],test_rez)
print "Precision:", mt.precision_score(test[:,2],test_rez)
print "Recall/sensitivity:", mt.recall_score(test[:,2],test_rez)


clf = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(100,20), random_state=1)
clf.fit(X,y)
Z = clf.predict(koef)
Z = Z.reshape(x_mg.shape)
test_rez = clf.predict(test[:,0:2])

plt.figure()
for element in test:
    if element[2]==1:
        plt.scatter(element[0], element[1], c='c')
    else:
        plt.scatter(element[0], element[1], c='orange')
plt.contour(x_mg, y_mg, Z, cmap=plt.cm.Paired)

c_matrix=mt.confusion_matrix(test[:,2],test_rez)
plot_confusion_matrix(c_matrix)

print "Accuracy:", mt.accuracy_score(test[:,2],test_rez)
print "Misclassification rate:", 1-mt.accuracy_score(test[:,2],test_rez)
print "Precision:", mt.precision_score(test[:,2],test_rez)
print "Recall/sensitivity:", mt.recall_score(test[:,2],test_rez)


clf=lm.LogisticRegression()

poly = PolynomialFeatures(degree=2, include_bias = False)
trening_new = poly.fit_transform(trening[:,0:2])


clf.fit(trening_new,y)
Z = clf.predict(poly.fit_transform(koef))
Z = Z.reshape(x_mg.shape)
test_rez = clf.predict(poly.fit_transform(test[:,0:2]))

plt.figure()
for element in test:
    if element[2]==1:
        plt.scatter(element[0], element[1], c='c')
    else:
        plt.scatter(element[0], element[1], c='orange')
plt.contour(x_mg, y_mg, Z, cmap=plt.cm.Paired)

c_matrix=mt.confusion_matrix(test[:,2],test_rez)
plot_confusion_matrix(c_matrix)

print "Accuracy:", mt.accuracy_score(test[:,2],test_rez)
print "Misclassification rate:", 1-mt.accuracy_score(test[:,2],test_rez)
print "Precision:", mt.precision_score(test[:,2],test_rez)
print "Recall/sensitivity:", mt.recall_score(test[:,2],test_rez)
