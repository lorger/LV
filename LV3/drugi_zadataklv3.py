# -*- coding: utf-8 -*-
"""
Created on Thu Nov 24 21:25:09 2016

@author: Korisnica
"""

import pandas as pd
import matplotlib.pyplot as plt
mtcars = pd.read_csv('mtcars.csv')

#Zadatak 2.1 Prikažite na istoj slici potrošnju automobila s 4, 6 i 8 cilindara.


mtcars[mtcars.cyl==4].mpg.plot(kind='bar', stacked=True, color='r', label='4 cilindra')  #crtanje bar plota po cilindrima
mtcars[mtcars.cyl==6].mpg.plot(kind='bar', stacked=True, color='c', label='6 cilindara')
mtcars[mtcars.cyl==8].mpg.plot(kind='bar', stacked=True, color='b', label='8 cilindra')
plt.title('Potrosnja automobila')
plt.legend()
plt.show()


#Zadatak 2.2 Prikažite na istoj slici distribuciju težine automobila s 4, 6 i 8 cilindara


fig,ax=plt.subplots()
ax = mtcars[mtcars.cyl==4].mpg.plot(kind='box', stacked=True, color='r',positions=[1] )  #crtanje bar plota po cilindrima
mtcars[mtcars.cyl==6].mpg.plot(kind='box', stacked=True, color='r',  positions=[2])
mtcars[mtcars.cyl==8].mpg.plot(kind='box', stacked=True, color='b',  positions=[3])
ax.set_xticks(range(4))
ax.set_xticklabels(['','4 cilindra', '6 cilindara', '8 cilindara'])
plt.title('Tezina cilindara')
plt.show()


#Zadatak 2.3 Pomoću odgovarajućeg grafa pokušajte odgovoriti na pitanje imaju li automobili s ručnim mjenjačem veću
#potrošnju od automobila s automatskim mjenjačem?

fig,ax=plt.subplots()
mtcars[mtcars.am==1].mpg.plot(kind='box', color='c', positions=[1] )
mtcars[mtcars.am==0].mpg.plot(kind='box', color='r', positions=[2])
ax.set_xticks(range(4))
ax.set_xticklabels(['','Rucni mjenjac', 'Automatski mjenjac'])
plt.show()

 #crtanje bar plota po vrsti mjenjaca i prosjecnoj potrosnji

#Zadatak 2.4 Prikažite na istoj slici odnos ubrzanja i snage automobila za automobile s ručnim odnosno automatskim
#mjenjačem

plt.plot(mtcars[mtcars.am==1].qsec, mtcars[mtcars.am==1].hp, '^', label='Manual')
plt.plot(mtcars[mtcars.am==0].qsec, mtcars[mtcars.am==0].hp, 'o', label='Automatic')
plt.legend()
plt.show()