# -*- coding: utf-8 -*-
"""
Created on Thu Nov 24 20:25:09 2016

@author: Korisnica
"""


import pandas as pd
import numpy as np

mtcars=pd.read_csv('mtcars.csv')

#1)

max_potr=mtcars.sort_values(['mpg'],ascending=0)   #sortira silazno prema potrosnji goriva (mpg)
print "Pet automobila sa najvecom potrosnjom:"
print max_potr.head(5)

#2)

min_potr=mtcars[mtcars.cyl==8]     #automobili sa 8 cilindara
min_potr=mtcars.sort_values(['mpg'],ascending=1)  #uzlazno sortiranje 
print "Automobili sa najmanjom potrosnjom i 8 cilindara:"
print min_potr.head(3)

#3)

avg_potr=np.average(mtcars[mtcars.cyl==6]['mpg'])   #sr. potrosnja goriva auta koji imaju 6 cilindara
print "Srednja potrosnja automobila koji imaju 6 cilindara: ",avg_potr

#4)

sr_pot_6=np.average(mtcars[(mtcars.wt>2.2) & (mtcars.wt<2.4) & (mtcars.cyl==4)]['mpg'])   #srednja potrosnja goriva auta sa 4 cilindra i tezinom izmedju 2200 i 2400 lbs
print "\nSrednja potrosnja automobila koji imaju 4 cilindra i tezinu izmedju 2200 - 2400 lbs", sr_pot_6  

#5)

AT=len(mtcars[mtcars.am==1])   #broj automobila sa automatskim
MT=len(mtcars[mtcars.am==0])   #broj automobila sa manualom
print "Broj automobila sa automatskim mjenjacem:",AT
print "Broj automobila sa rucnim mjenjacem:",MT

#6)

at100hp=len(mtcars[(mtcars.am==1) & (mtcars.hp>100)]) #automobili sa AT i preko 100HP
print "Broj auta sa automatskih mjenjacem i preko 100HP:",at100hp

#7)

mtcars.wt=mtcars.wt*453.592 #1000lbs = 453kg
print " Automobili u kilogramima:"
print mtcars



