# -*- coding: utf-8 -*-
"""
Created on Mon Dec 19 16:43:53 2016

@author: Korisnica
"""
import pandas as pd
import urllib	
import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt


url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=4&vrijemeOd=01.01.2015&vrijemeDo=31.12.2015Test'
airQualityHR = urllib.urlopen(url).read()
root = ET.fromstring(airQualityHR)
df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))


i = 0
while True:
    
    try:
        obj = root.getchildren()[i].getchildren()
    except:
        break
    
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    df = df.append(row_s)
    df.mjerenje[i] = float(df.mjerenje[i])
    i = i + 1
    
    
df.vrijeme = pd.to_datetime(df.vrijeme)   # adding date month and day designator
df.plot(y='mjerenje', x='vrijeme');

df['mjesec'] = df['vrijeme'].dt.month
df['day_of_week'] = df['vrijeme'].dt.dayofweek


max_cont=df.sort_values('mjerenje', ascending=False)
print max_cont.head(3)


plt.figure()
for i in range(1,13):
    m=df[df.month==i].mjerenje.mean()
    plt.bar(i, m)
    

plt.figure()
fig,ax=plt.subplots()
df[df.month==7].mjerenje.plot( kind="box", color='b')
df[df.month==1].mjerenje.plot( kind="box", color='r', positions=[2])
ax.set_xticks(range(3))
ax.set_xticklabels(['','srpanj', 'sijecanj'])


plt.figure()
fig,ax=plt.subplots()
df[df.dayOfweek<=4].mjerenje.plot(kind="box", color='b')
df[df.dayOfweek>4].mjerenje.plot(kind="box", color='r', positions=[2])
ax.set_xticks(range(3))
ax.set_xticklabels(['','radni dan', 'vikend'])