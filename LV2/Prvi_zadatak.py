# -*- coding: utf-8 -*-
"""
Created on Wed Nov 16 22:17:52 2016

@author: Korisnica
"""

#Zadatak1 - Napišite Python skriptu koja će učitati tekstualnu datoteku, pronaći valjane email adrese te izdvojiti samo prvi dio
#adrese (dio ispred znaka @). Koristite odgovarajući regularni izraz. Koristite datoteku mbox-short.txt. Ispišite rezultat. 

import re  #za regularne izraze

a = raw_input('ime datoteke koju zelite otvoriti: ')

try:
    datoteka = open(a)
except:
    print 'Nemoguce otvoriti datoteku.', a
    exit()
    
print 'Rezultat:'

for linija in datoteka:
    linija=linija.rstrip()
    lst=re.findall('([\w\.-]+)@[\w\.-]+', linija)  #svi redovi u kojima se nalazi @ te se ispise samo dio ispred @
    if( len(lst)>0):
        print lst  #ispis rezultata
