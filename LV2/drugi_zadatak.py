# -*- coding: utf-8 -*-
"""
Created on Wed Nov 16 23:01:28 2016

@author: Korisnica
"""

#Zadatak2


import numpy as np #biblioteka kao u trecem i cetvrtom zadataku
import matplotlib.pyplot as plt


br_osoba = input("Duzina vektora: ")   #broj osoba       
spol=np.random.random_integers(0, 1, int(br_osoba))     #generiraj random 0 i 1

print spol

visine =np.zeros(int(br_osoba))

for a in range(0,len(spol)):
    if spol[a]==1:
        visine[a]=np.random.normal(180, 7)    #ako je 1 dodaj visinu 180 s devijacijom 7
        plt.plot(visine[a], 'b s') #plava boja
    else:
        visine[a]=np.random.normal(167, 7)    #ako je 0 dodaj visinu 167 s devijacijom 7
        plt.plot(visine[a], 'r o')  #crvena boja       
print visine


arit_muski=np.dot(visine, spol)/sum(spol)   #srednja vrijednost muskih osoba
arit_zene=np.dot(visine,(spol-np.ones(br_osoba)))/sum(spol-np.ones(br_osoba))   #srednja vrijednost zenskih osoba

plt.plot(arit_muski,'b ^')
plt.plot(arit_zene,'r x')

print arit_muski
print arit_zene