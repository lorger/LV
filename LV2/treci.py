# -*- coding: utf-8 -*-
"""
Created on Wed Nov 16 20:04:29 2016

@author: Korisnica
"""

#Zadatak3- Simulirajte 100 bacanja igraće kocke (kocka s brojevima 1 do 6). Pomoću histograma prikažite rezultat ovih bacanja.

import numpy as np #biblioteka za matematicke operacije (linearne, Fouriera itd)
import matplotlib.pyplot as plt

plt.xlabel("x os")
plt.ylabel("y os")

number=np.random.random_integers(1,6,100)  #generiranje 100 randomnih bacanja brojeva od 1 do 6

plt.hist(number, bins=[0, 1, 2, 3, 4, 5, 6, 7, 8 ])  #crtanje histograma, brojevi do 8 jer ljepše izgleda
