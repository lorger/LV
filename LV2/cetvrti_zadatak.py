# -*- coding: utf-8 -*-
"""
Created on Wed Nov 16 21:01:57 2016

@author: Korisnica
"""


#Zadatak_4 - Simulirajte bacanje igraće kocke 30 puta. Izračunajte srednju vrijednost rezultata i standardnu devijaciju. Pokus
#ponovite 1000 puta. Prikažite razdiobu dobivenih srednjih vrijednosti. Što primjećujete? Kolika je srednja vrijednost i
#standardna devijacija dobivene razdiobe? Što se događa s ovom razdiobom ako pokus ponovite 10000 puta? 


import numpy as np #kao i u prošlom zadatku
import matplotlib.pyplot as plt


srednja=np.zeros(1000)     #srednja vrijednost,ponavlja se 1000 puta, kao i u proslom zadatku
stan_dev=np.zeros(1000)      #stan. devijacija
for a in range(0,1000):
    number=np.random.random_integers(1,6,30)   #generira 30 nasumicnih bacanja brojeva od 1 do 6
    
    srednja[a]=np.average(number)
    stan_dev[a]=np.std(number)
    
plt.hist(srednja)   #crta srednju vrijednost
print stan_dev   #ispisuje stan.derivaciju
plt.hist (stan_dev)
    
