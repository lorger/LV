# -*- coding: utf-8 -*-
"""
Created on Mon Nov 07 20:03:48 2016

@author: Korisnica
"""

brojac=0  #za koliko brojeva je korisnik unio 
suma=0
srednja=0 #srenja vrijednost
minimum=0  #minimalna vrijednost
maksimum=0 #maksimalna vrijednost

while 1:
    a=raw_input("Unesite zeljeni broj: ")    #unosenje zeljenog broja
    try:
        b=float(a)     # provjerava da li je tocno unesen broj
        if(brojac==0):   #broj koji je prvi unesen postati ce minimalni broj
            minimum=b
        if(b<minimum): #ako je novi uneseni broj manji od minimuma, postaje minimum
            minimum=b
        if(b>maksimum):
            maksimum=b #ako je novi uneseni broj manji od maksimuma, postaje maksimum
        suma=suma+b 
        brojac=brojac+1
        
        
    except ValueError:
        if(a=="Done"):     #upis 'Done' zavrsava petlju
            break
        else:
            print "Niste upisali broj"
            
        
srednja=suma/brojac    #izracunavanje srednje vrijednosti
print "Uneseno: ", brojac, "brojeva."
print "Najmanji broj: ", minimum
print "Najveci broj: ", maksimum
print "Srednja vrijednost: ", srednja

            
        
